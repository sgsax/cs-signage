<?php

// see http://jquery.malsup.com/cycle/ for full documentation on using the
//  image rotation applet

//// custom settings here
// name and location of your default placeholder image
$default_img = "images/default.png";
// location of your overrrides folder
$overrides = "images/overrides";
// name and relative location of the database file
$db = "signage.db";       
// page title, typically won't see this in deployment
$title = "My Digital Sign";     
// frequency for page reloading to get new image sets
$refreshseconds = 1800;   
// animation for slide transitions
$transitionfx = "none";
// default hostname
$hostname = "sign1";
//// end custom settings

// hostname can be passed in as an arg, handle that case here
$host = (isset($_GET['client']) ? $_GET['client'] : $hostname);

// specify the database we'll be querying, used for other query funcs later
class sqliteDB extends SQLite3
{
    function __construct($db)
    {
        $this->open($db);
    }
}

// perform a db lookup with the hostname to get a list of image directories
//   that should be read by this client, return them in an array
function get_imgdirs($hostname, $db) {
    $ret = array();

    $sql="select s.name,s.path,c.hostname from slide_sets s inner join clients_slides cs on s.id=cs.slides_id inner join clients c on cs.client_id=c.id where c.hostname=\"$hostname\";";
    
    $results = db_query($sql, $db);
    
    foreach ($results as $row) {
        array_push($ret, $row['path']);
    }

    return $ret;
}

// get a list of all valid image files in all directories to be displayed,
//    return them in an array; if there are any overrides, don't return
//    anything else
function get_imgs($paths, $ovrd) {
    $imgs = array();

    // get override images list first
    if (in_array($ovrd, $paths)) {
        $imgs = get_img_list($ovrd);
    }

    // if we don't have any overrides, get a full image list
    if (count($imgs) == 0) {
        foreach ($paths as $path) {
            $imgs = array_merge($imgs, get_img_list($path));
        }
    }

    // sort the image list
    //natcasesort($imgs);
    
    // randomize the list
    shuffle($imgs);

    return $imgs;
}

// get a list of images in a single directory, return them in an array
function get_img_list($path) {
    $imgs = array();

    if ($hnd = opendir($path)) {
        while  (false != ($img = readdir($hnd))) {
//            if ($img != "archive" && !( preg_match("/^\..*/", $img))) {
            if (is_image("$path/$img")) {
                array_push($imgs, "$path/$img");
            }
        }
        closedir($hnd);
    }

    return $imgs;
}

// perform a db lookup with the hostname to get a list of corner overlay URLs
//   that should be displayed by this client, return them in an array
function get_overlays($hostname, $db) {
    $ret=array();

    $sql="select c.hostname, l.desc, o.url from overlays o inner join clients c on o.client_id=c.id inner join overlay_locations l on o.location_id=l.id where c.hostname=\"$hostname\";";

    $ret = db_query($sql, $db);
    
    return $ret;
   
}

// lookup the hostname in the db and find out if it has a custom timeout for
//   the image rotation period
function get_timeout($hostname, $db) {
    $sql="select custom_timeout from clients where hostname=\"$hostname\";";

    $result = db_query($sql, $db);

    // there should only be one result
    $ret = $result[0]['custom_timeout'];

    if ($ret == 0) {
        $ret = 15000;    // set default value of 15 sec if not specified in db
    }

    return $ret;
}

// generic function to run a query on the db and return the results as an array
function db_query($sql, $db) {
    $ret = array();

    $sqlitedb = new sqliteDB($db);

    $results = $sqlitedb->query($sql);

    while($row = $results->fetchArray(SQLITE3_ASSOC) ){
        array_push($ret, $row);
    }

    $sqlitedb->close();
    return $ret;
}

// check the file's mimetype to see if it's an image or not
function is_image($file) {
    $imgtype = mime_content_type($file);
    return (preg_match("/image/", $imgtype) && (preg_match("/png/", $imgtype) || preg_match("/jpeg/", $imgtype) || preg_match("/gif/", $imgtype)));
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<title><?php print $title; ?></title>

  <link href="signage.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
  <!-- include Cycle plugin -->
  <script type="text/javascript" src="http://malsup.github.io/jquery.cycle.all.js"></script>
  <!-- set a few parameters on the image cycler, calculate screen area and
       resize accordingly -->
  <script type="text/javascript">
    $(document).ready(function() { 
        $('.slideshow').cycle({
            fx: 'none',
            speed: 500,
            timeout: <?php echo get_timeout($host, $db); ?>
        });
        var adjHeight = $(window).height() - 16;
        $('.slideshow').height(adjHeight);
        $('.slideshow div').height(adjHeight);
        $('.slideshow img').height(adjHeight);
    });
  </script>

      <meta http-equiv="refresh" content="<?php print $refreshseconds; ?>">
</head>
<body>
<div class="slideshow">

<?php
// setup the images to be displayed
$paths = get_imgdirs($host, $db);

// if we have any images for this host, add them to the cycler here, otherwise
//    display a placeholder
if (count($paths) > 0) {
    foreach (get_imgs($paths, $overrides) as $img) {
        print '<div><img src="' . $img . '" /></div>' . "\n";
    }
} else {
    print '<div><img src="' . $default_img . '" /></div>' . "\n";
}
?>
</div>

<?php
// setup overlay panels
$overlays = get_overlays($host, $db);

// don't bother unless any should be applied
if (count($overlays) > 0) {
    foreach ($overlays as $overlay) {
        print '<div class="overlay" id="' . $overlay['desc'] . '">'. "\n";
        print '    <div class="wrapper">' . "\n";
        print '        <iframe id="overlayframe" src="' . $overlay['url'] . '"></iframe>' . "\n";
        print '    </div>' . "\n";
        print '</div>' . "\n";
    }
}
?>

<!-- display unobtrusive hostname for debugging purposes -->
<div class="overlay" id="hostname"><?php echo $host; ?></div>

</body>
</html>

#!/bin/bash

# set the local DISPLAY if running remotely
if [[ -z $DISPLAY ]]; then
    export DISPLAY=:0
fi

BROWSER=google-chrome
BROWSEROPTS="--start-fullscreen --noerrdialogs"
PROFILEDIR=$HOME/.config/google-chrome/Default
CACHEDIR=$HOME/.cache/google-chrome/Default/Cache

# cleanup files to remove error messages on start
sed -i 's/"exited_cleanly":*false/"exited_cleanly":true/' $PROFILEDIR/Preferences
sed -i 's/"exit_type":*"Crashed"/"exit_type":"None"/' $PROFILEDIR/Preferences
rm -rf $PROFILEDIR/Current* $PROFILEDIR/Last* $CACHEDIR/*

$BROWSER $BROWSEROPTS "http://signage.cs.ksu.edu/\?client=$(hostname)" >/dev/null 2>&1 &
sleep 10
xte "key F5" -x:0

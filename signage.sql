CREATE TABLE clients(
id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
hostname CHAR(50),
active BOOLEAN DEFAULT 1
, custom_timeout BIGINT DEFAULT 0);

CREATE TABLE slide_sets(
id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
name CHAR(50),
path CHAR(255)
);

CREATE TABLE clients_slides(
id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
client_id INTEGER,
slides_id INTEGER,
FOREIGN KEY(client_id) REFERENCES clients(id),
FOREIGN KEY(slides_id) REFERENCES slide_sets(id)
);

CREATE TABLE overlay_locations(
id integer primary key not null,
desc char(50)
);

CREATE TABLE overlays(
id integer primary key autoincrement not null,
client_id integer,
location_id integer,
url char(512),
foreign key (client_id) references clients(id),
foreign key (location_id) references overlay_locations(id)
);

